<?php
session_start();
?>

<!DOCTYPE html>

<html>
<head>
    <title>Centrale pagina</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css"
          integrity="sha384-1q8mTJOASx8j1Au+a5WDVnPi2lkFfwwEAa8hDDdjZlpLegxhjVME1fgjWPGmkzs7" crossorigin="anonymous">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap-theme.min.css"
          integrity="sha384-fLW2N01lMqjakBkx3l/M9EahuwpSfeNvV63J5ezn3uZzapT0u7EYsXMjQV+0En5r" crossorigin="anonymous">
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"
            integrity="sha384-0mSbJDEHialfmuBBQP6A4Qrprq5OVfW37PRR3j5ELqxss1yVqOtnepnHVP9aJ7xS"
            crossorigin="anonymous"></script>

    <script
        src="https://code.jquery.com/jquery-2.2.3.min.js"
        integrity="sha256-a23g1Nt4dtEYOj7bR+vTu7+T8VP13humZFBJNIYoEJo="
        crossorigin="anonymous"></script>


    <link rel="stylesheet" type="text/css" href="css/style.css">
    <link type="image/x-icon" href="favicon.ico" rel="shortcut icon">
    <link rel="apple-touch-icon" href="favicon.png">

    <script>
        $(document).ready(function () {
            //Prestudy
            $("#prestudy").val("");
            $("#prestudy").bind("change", function () {
                if ($(this).val() == "other") {
                    $("#other").slideDown();
                }
                else {
                    $("#other").slideUp();
                }
            });

            //Address
            $("#brochurePDF").prop("checked", true);
            $("input[name='brochureType']").change(function (e) {
                if ($(this).val() == "1") {
                    $("#address").slideDown();
                }
                else {
                    $("#address").slideUp();
                }

            });
        });
    </script>

    <?php
    include_once('include/database_connect.php');
    include_once('include/check_function.php');
    include_once('include/create_function.php');
    include_once('phpmailer/PHPMailerAutoload.php');
    ?>

</head>
<body>
<?php

$errors = array();

$url = 'http' . (isset($_SERVER['HTTPS']) ? 's' : '') . '://' . "{$_SERVER['HTTP_HOST']}";

$ipaddress = $_SERVER['REMOTE_ADDR'];
$datetime = strtotime("now");
$code = '$srGM&2@uyV8#4PE-q3x4b+Q&TR7fL8n';

$specialisationDB = $dbh->prepare("SELECT * FROM `specialisation`");
$specialisationDB->execute();
$specialisationsArray = $specialisationDB->fetchAll(PDO::FETCH_KEY_PAIR);

$studiesDB = $dbh->prepare("SELECT `study_id`, `study` FROM `study` WHERE `specialisation_id` <> :spid");
$studiesDB->execute(array(':spid' => 0));
$studiesArray = $studiesDB->fetchAll(PDO::FETCH_KEY_PAIR);

$graduationYearsArray = array(
    date("Y", strtotime("-1 year")),
    date("Y", strtotime("now")),
    date("Y", strtotime("+1 year")),
    'completed' => 'Afgerond'
);

$preStudiesArray = array(
    1 => 'Havo',
    2 => 'Vwo',
    3 => 'Mbo',
    'other' => 'Anders, nl.'
);


if (!empty($_POST)) {
    $firstname = htmlspecialchars(trim($_POST['firstname']));
    $prefix = htmlspecialchars(trim($_POST['prefix']));
    $surname = htmlspecialchars(trim($_POST['surname']));
    $email = htmlspecialchars(trim($_POST['email']));

    $prestudy = htmlspecialchars(trim($_POST['prestudy']));
    $prestudyother = htmlspecialchars(trim($_POST['prestudyOther']));
    $graduationyear = htmlspecialchars(trim($_POST['graduationyear']));

    $brochuretype = htmlspecialchars(trim($_POST['brochureType']));

    $zipcode = htmlspecialchars(trim($_POST['zipcode']));
    $number = htmlspecialchars(trim($_POST['number']));
    $street = htmlspecialchars(trim($_POST['street']));
    $city = htmlspecialchars(trim($_POST['city']));

    if (!IsName($firstname)) {
        $errors['firstname'] = "Voornaam is niet geldig.";
    }

    if ($prefix != "") {
        if (!IsName($prefix)) {
            $errors['prefix'] = "Tussenvoegsel is niet geldig.";
        }
    }

    if (!IsName($surname)) {
        $errors['surname'] = "Achternaam is niet geldig.";
    }


    if (!IsEmail($email)) {
        $errors['email'] = "Email is niet geldig.";
    }

    if ($prestudy != "") {
        if (!array_key_exists($prestudy, $preStudiesArray)) {
            $errors['prestudy'] = "Vooropleiding is niet geldig.";
        } else {
            if ($prestudy == "other") {
                if (!IsBetween($prestudyother)) {
                    $errors['prestudy'] = "Vooropleiding anders is niet geldig.";
                }
            }
        }
    }

    if ($graduationyear != "") {
        if (!in_array($graduationyear, $graduationYearsArray)) {
            $errors['graduationyear'] = "Afstudeerjaar is niet geldig.";
        } else {
            if ($graduationyear == 'Afgerond') $graduationyear = 0;
        }
    }

    if ($brochuretype == 1) {
        if (!IsPostal($zipcode)) $errors['zipcode'] = "Postcode is niet geldig (1337XX).";
        if ((!IsAlphaNumber($number)) || ($number == "")) $errors['number'] = "Huismnummer is niet geldig.";
        if (!IsName($street)) $errors['street'] = "Straat is niet geldig.";
        if (!IsName($city)) $errors['city'] = "Stad is niet geldig.";

    } else {
        if ($brochuretype != 2) {
            $errors['brochuretype'] = "Geen geldige manier om de brochure(s) te versturen.";
        }
    }

    if (isset($_POST['studies'])) {
        $studies = $_POST['studies'];
        foreach ($studies as $study) {
            if (!array_key_exists($study, $studiesArray)) {
                $errors['studies'] = "Ongeldige studie aanwezig.";
                break;
            }
        }
    } else {
        $studies = array();
        $errors['studies'] = "Selecteer de studies waarin jij geïntereseerd bent.";
    }

    if (empty($errors)) {
        //Check prestudy
        $checkPreStudyOther = $dbh->prepare("SELECT `study_id` FROM `study` WHERE `study` = :prestudyother");
        $checkPreStudyOther->execute(array(':prestudyother' => $prestudyother));

        if ($checkPreStudyOther->rowCount() == 1) {
            $preStudyID = $checkPreStudyOther->fetch();
            $preStudyID = $preStudyID['study_id'];
        } else {
            $dbh->beginTransaction();
            $insertPreStudyOther = $dbh->prepare("INSERT INTO `study` (`study` , `specialisation_id`)
                                                      VALUES (:prestudyother, 0)");
            $insertPreStudyOther->execute(array(':prestudyother' => $prestudyother));
            $preStudyID = $dbh->lastInsertId();
            $dbh->commit();
        }

        //Check address
        if ($brochuretype == 1) {
            $checkAddress = $dbh->prepare("SELECT `address_id` FROM `address` WHERE `zipcode` = AES_ENCRYPT(:zipcode, :code) AND `number` = AES_ENCRYPT(:number, :code)");
            $checkAddress->execute(array(':zipcode' => $zipcode, ':number' => $number, ':code' => $code));


            if ($checkAddress->rowCount() == 1) {
                $addressID = $checkAddress->fetch();
                $addressID = $addressID['address_id'];
            } else {
                $dbh->beginTransaction();
                $insertAddress = $dbh->prepare("INSERT INTO `address` (`zipcode`, `number`, `street`, `city`)
                                                VALUES (AES_ENCRYPT(:zipcode, :code), AES_ENCRYPT(:number, :code), 
                                                AES_ENCRYPT(:street, :code), AES_ENCRYPT(:city, :code))");
                $insertAddress->execute(array(':zipcode' => $zipcode, ':number' => $number,
                    ':street' => $street, ':city' => $city, ':code' => $code));
                $addressID = $dbh->lastInsertId();
                $dbh->commit();
            }
        } else {
            $addressID = 0;
        }

        //Check person
        $checkPerson = $dbh->prepare("SELECT `person_id` FROM `person` WHERE `firstname` = AES_ENCRYPT(:firstname, :code) AND 
                                                                    `prefix` = AES_ENCRYPT(:prefix, :code) AND  
                                                                    `surname` = AES_ENCRYPT(:surname, :code) AND 
                                                                    `email` = AES_ENCRYPT(:email, :code)");
        $checkPerson->execute(array(':firstname' => $firstname, ':prefix' => $prefix, ':surname' => $surname, 'email' => $email,
            ':code' => $code));

        if ($checkPerson->rowCount() == 1) {
            $personID = $checkPerson->fetch();
            $personID = $personID['person_id'];
        } else {
            $dbh->beginTransaction();
            $insertPerson = $dbh->prepare("INSERT INTO `person` (`firstname`, `prefix`, `surname`,
                                                  `email`) VALUES 
                                                   (AES_ENCRYPT(:firstname, :code), AES_ENCRYPT(:prefix, :code),
                                                   AES_ENCRYPT(:surname, :code), AES_ENCRYPT(:email, :code))");

            $insertPerson->execute(array(':firstname' => $firstname, ':prefix' => $prefix, ':surname' => $surname, 'email' => $email,
                ':code' => $code));
            $personID = $dbh->lastInsertId();
            $dbh->commit();
        }

        //Insert request
        $dbh->beginTransaction();
        $insertRequest = $dbh->prepare('INSERT INTO `request` (`person_id`, `address_id`, `ipaddress`, `datetime`, `prestudy_id`, `graduation`) VALUES
                                         (:person_id, :address_id, AES_ENCRYPT(:ipaddress, :code), :datetime, :prestudy_id, :graduationyear)');
        $insertRequest->execute(array(':person_id' => $personID, ':address_id' => $addressID, ':datetime' => $datetime, ':ipaddress' => $ipaddress,
            ':prestudy_id' => $preStudyID, ':graduationyear' => $graduationyear, ':code' => $code));

        $requestID = $dbh->lastInsertId();
        $dbh->commit();

        foreach ($studies as $study) {
            $insertRequestPerson = $dbh->prepare("INSERT INTO `study_has_request` (`study_id`, `request_id`) VALUES (:study_id, :request_id)");
            $insertRequestPerson->execute(array(':request_id' => $requestID, ':study_id' => $study));
        }

        //Check newspaper
        if (isset($_POST['newspaper'])) {
            $checkNewsLetter = $dbh->prepare("SELECT `newsletter_id` FROM `newsletter` WHERE `email` = AES_ENCRYPT(:email, :code)");
            $checkNewsLetter->execute(array(':email' => $email, ':code' => $code));

            if ($checkNewsLetter->rowCount() != 1) {
                $insertNewsLetter = $dbh->prepare("INSERT INTO `newsletter` (`email`) VALUES (AES_ENCRYPT(:email, :code))");
                $insertNewsLetter->execute(array(':email' => $email, ':code' => $code));
            }
        }

        $_SESSION['successMessage'] = "Bedankt voor je brochureaanvraag, afhankelijk van je keuze ontvang je de brochure per e-mail of per post. Controleer eventueel je spamfolder.";

        if($brochuretype == 2){
            $mailMessage = "<html><body>
Beste {$firstname} {$surname},

<p>Bedankt voor je brochureaanvraag bij Windesheim! Via de onderstaande link(s) kun je je persoonlijke brochure(s) downloaden</p>
{$url}
<p>We hopen dat deze brochure(s) je helpt in je zoektocht naar een geschikte studie. Op <a href=\"http://www.windesheim.nl\">onze website</a> geven wij je tips over hoe je een studie kunt kiezen.</p>
<p>Om een compleet beeld te krijgen van de opleiding(en) en van Windesheim nodigen we je van harte uit voor onze open dagen. de eerstvolgende open dag is op 11 november 2016. klik <a href=\"http://www.windesheim.nl/studeren/studie-kiezen/verzamel/open-dag/\">hier</a> voor meer informatie.</p>
<p>heb je al een studie gevonden die echt bij je past? of misschien meerdere? volg dan een <a href=\"http://www.windesheim.nl/studeren/studie-kiezen/verzamel/meeloopdag/\">meeloopdag</a>
 om te ervaren hoe het nou echt is om te studeren aan de opleiding van je keuze.</p>
<p>Vragen? neem dan contact op met het Windesheim Informatiecentrum: 0900-8899 (lokaal tarief) of per mail op <a href='mailto:info@windesheim.nl'>info@windesheim.nl</a></p>
<p>Met vriendelijke groet,</p>
<p>Windesheim Serviceplein - Informatiecentrum<br/>
Campus 2-6 | Postbus 10090 | 8000 GB Zwolle<br/>
Telefoon 0900 - 8899<br/>
<a href=\"www.windesheim.nl\">www.windesheim.nl</a> | <a href=\"mailto:info@windesheim.nl\">info@windesheim.nl</a></p>
</body></html>";

            $mail = new PHPMailer;

            $mail->isSMTP();                                      // Set mailer to use SMTP
            $mail->Host = 'madrid.windesheim.intra';  // Specify main and backup SMTP servers
            $mail->SMTPAuth = true;                               // Enable SMTP authentication
            $mail->Username = 'info@windesheim.intra';                 // SMTP username
            $mail->Password = 'KbS2DfGo1';                           // SMTP password
            $mail->SMTPSecure = 'ssl';                            // Enable TLS encryption, `ssl` also accepted
            $mail->Port = 465;                                    // TCP port to connect to

            $mail->setFrom('info@windesheim.intra', 'Windesheim Brochureaanvraag');
            $mail->addAddress($email, $firstname . " " . $lastname);
            $mail->isHTML(true);                                  // Set email format to HTML

            $mail->Subject = 'Informatieaanvraag Windesheim';
            $mail->Body    = $mailMessage;
            $mail->send();
        }

        die(header('location:index.php'));

    }
} else {
    $firstname = "";
    $prefix = "";
    $surname = "";
    $email = "";

    $prestudy = "";
    $prestudyother = "";
    $graduationyear = "";

    $zipcode = "";
    $number = "";
    $street = "";
    $city = "";

    $studies = array();
}

?>
<div class="container">

    <h1>Vraag een brochure aan</h1>
    <p><b>Wil je meer informatie over een opleiding? Vul dan onderstaand formulier in en ontvang de gewenste
            brochure.</b></p>
    <div class="reminder">
        <img src="img/lightbulb.png"/>
        <span>
             Dit formulier liever op een ander moment invullen?
            <br><a
                href="mailto:?subject=Reminder: formulier Windesheim invullen&amp;body=Niet vergeten om dit formulier in te vullen en te verzenden: http://www.windesheim.nl/studeren/studie-kiezen/verzamel/vraag-een-brochure-aan/%0D%0A%0D%0ABij vragen en meer info:%0D%0AWindesheim Informatiecentrum%0D%0A0900 - 8899 (telefonisch bereikbaar van maandag t/m vrijdag 9.00 - 17.00 uur)%0D%0Ainfo@windesheim.nl%0D%0ATwitter: @wh_webcare%0D%0Awww.windesheim.nl">Stuur
                jezelf een reminder</a>
        </span>
    </div>
    <br/><br/><br/><br/>
    <p> Ben je jonger dan 16 jaar? Vraag dan eerst toestemming aan een van je ouders of verzorgers om je persoonlijke
        gegevens hier achter te laten.</p>


    <?php
    if (!empty($errors)) {
        echo("<div class='alert alert-danger'>");
        echo("<strong>Let op!</strong> De volgende foutmeldingen zijn opgetreden:");
        echo("<ul>");
        foreach ($errors as $error) {
            echo("<li>{$error}</li>");
        }
        echo("</ul>");
        echo("</div>");
    }

    if (isset($_SESSION['successMessage'])) {
        echo("<div class='alert alert-success'>");
        echo("<strong>Gelukt!</strong> {$_SESSION['successMessage']}");
        echo("</div>");
        unset($_SESSION['successMessage']);
    } else {
        ?>
        <form class="form-horizontal" role="form" method="post">
            <fieldset>
                <legend>Vul je persoonlijke gegevens in</legend>
                <div class="form-group">
                    <label for="firstname" class="col-sm-5 control-label">Voornaam *</label>
                    <div class="col-sm-5">
                        <input type="text" name="firstname" value="<?php echo $firstname ?>" class="form-control">
                    </div>
                </div>
                <div class="form-group">
                    <label for="prefix" class="col-sm-5 control-label">Tussenvoegsel</label>
                    <div class="col-sm-5">
                        <input type="text" name="prefix" value="<?php echo $prefix ?>" class="form-control">
                    </div>
                </div>
                <div class="form-group">
                    <label for="surname" class="col-sm-5 control-label">Achternaam *</label>
                    <div class="col-sm-5">
                        <input type="text" name="surname" value="<?php echo $surname ?>" class="form-control">
                    </div>
                </div>

                <div class="form-group">
                    <label for="email" class="col-sm-5 control-label">Emailadres *</label>
                    <div class="col-sm-5">
                        <input type="text" name="email" value="<?php echo $email ?>" class="form-control">
                    </div>
                </div>
            </fieldset>

            <fieldset>
                <legend>Vooropleiding</legend>
                <div class="form-group">
                    <label for="prestudy" class="col-sm-5 control-label">Opleiding</label>
                    <div class="col-sm-5">
                        <select id="prestudy" name="prestudy" class="form-control">
                            <option value=""></option>
                            <?php
                            foreach ($preStudiesArray as $key => $preStudyArray) {
                                echo("<option value='{$key}'>{$preStudyArray}</option>");
                            }
                            ?>
                        </select>
                    </div>
                </div>
                <div id="other" style="display:none" class="form-group">
                    <label for="prestudyOther" class="col-sm-5 control-label">Anders * </label>
                    <div class="col-sm-5">
                        <textarea name="prestudyOther" class="form-control"><?php echo $prestudyother ?></textarea>
                    </div>
                </div>
                <div class="form-group">
                    <label for="graduationyear" class="col-sm-5 control-label">Afstudeerjaar</label>
                    <div class="col-sm-5">
                        <select name="graduationyear" class="form-control">
                            <option></option>
                            <?php
                            foreach ($graduationYearsArray as $graduationYearArray) {
                                echo("<option value='{$graduationYearArray}'>{$graduationYearArray}</option>");
                            }
                            ?>
                        </select>
                    </div>
                </div>
            </fieldset>

            <fieldset>
                <legend>Wil je de brochure per post ontvangen of direct downloaden?</legend>
                <div id="brochureType" class="form-group">
                    <label for="brochureType" class="col-sm-5 control-label">Ik wil de brochure</label>
                    <div class="col-sm-5">
                        <input type="radio" name="brochureType" value=1 id="brochurePost">
                        <label for="brochurePost">Per post</label>
                        <input type="radio" name="brochureType" value=2 id="brochurePDF" checked>
                        <label for="brochurePDF">Pdf Downloaden</label>
                    </div>
                </div>
                <div id="address" style="display:none">
                    <div class="form-group">
                        <label for="zipcode" class="col-sm-5 control-label">Postcode * </label>
                        <div class="col-sm-5">
                            <input type="text" name="zipcode" value="<?php echo $zipcode ?>" onchange="getLocation()"
                                   class="form-control">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="number" class="col-sm-5 control-label">Huisnummer * </label>
                        <div class="col-sm-5">
                            <input type="text" name="number" value="<?php echo $number ?>" class="form-control">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="street" class="col-sm-5 control-label">Straat * </label>
                        <div class="col-sm-5">
                            <input type="text" name="street" value="<?php echo $street ?>" class="form-control">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="city" class="col-sm-5 control-label">Stad * </label>
                        <div class="col-sm-5">
                            <input type="text" name="city" value="<?php echo $city ?>" class="form-control">
                        </div>
                    </div>
                </div>
            </fieldset>

            <fieldset>
                <legend>Kies de gewenste opleidingen</legend>
                <div class="form-group">
                    <div class="col-sm10">
                        <?php
                        foreach ($specialisationsArray as $key => $value) {
                            echo("<dt>{$value}</dt>");
                            echo("<dd><table class='table'>");
                            $spStudies = $dbh->prepare("SELECT `study_id`, `study` FROM `study` WHERE `specialisation_id` = :spid");
                            $spStudies->execute(array(':spid' => $key));

                            foreach ($spStudies->fetchAll() as $spStudy) {
                                echo("<tr><td><input type='checkbox' value='{$spStudy['study_id']}'");
                                if (in_array($spStudy['study_id'], $studies)) {
                                    echo(" checked ");
                                }
                                echo("name='studies[]' id='study_{$spStudy['study_id']}'>");
                                echo("<label for='study_{$spStudy['study_id']}' class='control-label'>{$spStudy['study']}</label></td></tr>");
                            }
                            echo("</table></dd>");
                        }
                        ?>
                    </div>
                </div>
            </fieldset>

            <fieldset>
                <legend>Aanmelden nieuwsbrief StudiEnieuws</legend>
                <div class="form-group">
                    <label for="newspaper" class="col-sm-4 control-label">Nieuwsbrief</label>
                    <div class="col-sm-6">
                        <input type="checkbox"
                            <?php
                            if (isset($_POST['newspaper'])) {
                                echo(" checked ");
                            }
                            ?>
                               name="newspaper"><label for="newspaper">Ja, ik ontvang graag de nieuwsbrief
                            StudiEnieuws</label>
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-sm-10">
                        Bovenstaande gegevens gebruiken we om de brochure van jouw keuze toe te sturen en om je
                        eventueel
                        uit te
                        nodigen voor voorlichtingsactiviteiten die daarop aansluiten zoals open dagen. Ook kunnen we je
                        vragen om een
                        enquête over de brochures in te vullen.
                        Je gegevens worden vertrouwelijk behandeld en niet voor andere doeleinden gebruikt dan
                        bovenstaande.
                    </div>
                </div>
            </fieldset>
            <input type="submit" class="btn btn-default" value="Verzenden">
        </form>
        <?php
    }
    ?>
</div>
<footer>
    <?php echo "&copy;" . date("Y", strtotime("now")) . " " . gethostname() ?>
</footer>
</body>
</html>
