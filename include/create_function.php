<?php
/**
 * Created by PhpStorm.
 * User: Justin
 * Date: 24-11-2015
 * Time: 15:35
 */

include_once 'database_connect.php';

function CreateSession($username, $ipaddress, $salt)
{
	$randomWord = "";
	for ($i = rand(15, 20); $i > 0; $i--)
	{
		$randomWord .= CreateRandomChar();
	}

	if (session_id() == '')
	{
		session_start();
	}

	$_SESSION['userdata'] = CreateHash($username);
	$_SESSION['session'] = CreateHash($salt . CreateHash($ipaddress) . CreateHash($randomWord));

	return (CreateHash($_SESSION['userdata'] . $_SESSION['session']));
}

function CreatePasswordSession()
{
	$randomWord = "";
	for ($i = rand(15, 20); $i > 0; $i--)
	{
		$randomWord .= CreateRandomChar();
	}

	return (md5($randomWord));
}

function CreateRandomChar()
{
	$randomCharacters = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890!@#$%^&*()_+=-[]{};:|/.,?";
	return ($randomCharacters[rand(0, strlen($randomCharacters) - 1)]);
}

function CreateTimeFormat($seconds)
{
	$time = array('second' => $seconds);
	$output = "";

	$time['minute'] = floor($time['second'] / 60);
	$time['second'] = $time['second'] % 60;

	$time['hour'] = floor($time['minute'] / 60);
	$time['minute'] = $time['minute'] % 60;

	$time['day'] = floor($time['hour'] / 24);
	$time['hour'] = $time['hour'] % 24;

	$time = array_reverse($time);
	foreach ($time as $key => $value)
	{
		if ($key == 'day')
		{
			if ($value != 0)
			{
				if ($value == 1)
				{
					$output .= "1 dag ";
				}
				else
				{
					$output .= "{$value} dagen ";
				}
			}
		}

		if ($key == 'hour')
		{
			if ($value != 0)
			{
				if ($value == 1)
				{
					$output .= "1 uur ";
				}
				else
				{
					$output .= "{$value} uren ";
				}
			}
		}

		if ($key == 'minute')
		{
			if ($value != 0)
			{
				if ($value == 1)
				{
					$output .= "1 minuut ";
				}
				else
				{
					$output .= "{$value} minuten ";
				}
			}
		}

		if ($key == 'second')
		{
			if ($value != 0)
			{
				if ($value == 1)
				{
					$output .= "1 seconde ";
				}
				else
				{
					$output .= "{$value} seconden ";
				}
			}
		}
	}

	return ($output);
}

function CreateSalt()
{
	$salt = "";
	for ($i = rand(100, 128); $i > 0; $i--)
	{
		$salt .= CreateRandomChar();
	}

	return (CreateHash($salt));
}

function CreateHash($word)
{
	return (hash('sha512', $word));
}

function CreateFunction($rights)
{
	$functions = array(1 => 'Redacteur', 2 => 'Beheerder', 3 => 'Eigenaar');
	return ($functions[$rights]);
}

function DrawTimeFormat($time)
{
	if (strtoupper(substr(PHP_OS, 0, 3)) === 'WIN')
	{
		setlocale(LC_ALL, 'nld_nld');
	}
	else
	{
		setlocale(LC_ALL, 'nl_NL');
	}
	return (strftime('%#d %B %Y om %#H:%M', $time));

}
