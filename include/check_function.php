<?php

/**
 * Created by PhpStorm.
 * User: Justin_Vermeij
 * Date: 18-11-2015
 * Time: 20:14
 */

include_once 'create_function.php';
include_once 'database_connect.php';


    //Validators
    function IsInteger($str){
        if(preg_match_all( "/[0-9]/", $str, $result ) == strlen($str)) return true;
        return false;
    }

    function IsDouble($str){
        if(preg_match_all( "/[0-9.]/", $str, $result ) == strlen($str)) return true;
        return false;
    }

    function IsAlpha($str) {
        if(preg_match_all( "/[a-zA-Z]/", $str, $result ) == strlen($str)) return true;
        return false;
    }

    function IsAlphaDash($str) {
        if(preg_match_all( "/[a-zA-Z_]/", $str, $result ) == strlen($str)) return true;
        return false;
    }

	function IsAlphaSpace($str) {
		if(preg_match_all( "/[a-zA-Z ]/", $str, $result ) == strlen($str)) return true;
		return false;
	}

    function IsAlphaNumber($str) {
        if(preg_match_all( "/[a-zA-Z0-9]/", $str, $result ) == strlen($str)) return true;
        return false;
    }

    function IsAlphaDashNumber($str) {
        if(preg_match_all( "/[a-zA-Z_0-9]/", $str, $result ) == strlen($str)) return true;
        return false;

    }

	//entry validation
	function IsPhone($str){
		if(preg_match("/^\+[0-9]{11}$/", $str )) return true;
		return false;
	}

    function IsEmail($str) {
	    if(preg_match("/^(([a-zA-Z]|[0-9])|([-]|[_]|[.]))+[@](([a-zA-Z0-9])|([-])){2,63}[.](([a-zA-Z0-9]){2,63})+$/", $str )) return true;

        return false;
    }

	function IsPostal($str){
		if(preg_match( "/^[1-9][0-9]{3}[A-z]{2}$/", $str )) return true;
		return false;
	}

    function IsName($str){
        if(preg_match('/^[-_A-z ]{2,12}$/', $str)) return true;
        return false;
    }


	//number validation
    function IsBetween($str, $min = 3, $max = 25){
        if((strlen($str)>=$min) && (strlen($str)<=$max)) return true;
        return false;
    }

	function IsMax($str, $max = 25){
		if(strlen($str)<=$max) return true;
		return false;
	}
